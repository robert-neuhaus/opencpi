\iffalse
This file is protected by Copyright. Please refer to the COPYRIGHT file
distributed with this source distribution.

This file is part of OpenCPI <http://www.opencpi.org>

OpenCPI is free software: you can redistribute it and/or modify it under the
terms of the GNU Lesser General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program. If not, see <http://www.gnu.org/licenses/>.
\fi

%----------------------------------------------------------------------------------------
% Required document specific properties
%----------------------------------------------------------------------------------------
\def\comp{file\_{}write}
\def\Comp{File Write}
\def\docTitle{\Comp{} Component Data Sheet}
\def\snippetpath{../../../../../doc/av/tex/snippets}
%----------------------------------------------------------------------------------------
% Global latex header (this must be after document specific properties)
%----------------------------------------------------------------------------------------
\input{\snippetpath/LaTeX_Header}
\graphicspath{{figures/}}
%----------------------------------------------------------------------------------------

\begin{document}
\maketitle
\thispagestyle{empty}
\newpage

\begin{center}
  \textit{\textbf{Revision History}}
\end{center}
\begin{longtable}{|p{\dimexpr0.15\textwidth-2\tabcolsep\relax}
                  |p{\dimexpr0.65\textwidth-2\tabcolsep\relax}
                  |p{\dimexpr0.2\textwidth-2\tabcolsep\relax}|}
  \hline
  \rowcolor{blue}
  \textbf{Revision} & \textbf{Description of Change} & \textbf{Date} \\
  \hline
  v1.4 & Initial release. & 9/2019 \\
  \hline
  v1.5 & Changed all readable properties to readback.  Also updated the end of file descriptions with smart wrapper updates.  & 4/2019 \\
  \hline
\end{longtable}
\newpage

\def\name{\comp}
\def\workertype{Application}
\def\version{\ocpiversion}
\def\releasedate{4/2019}
\def\componentlibrary{ocpi.core}
\def\workers{file\_{}write.hdl file\_{}write.rcc}
\def\testedplatforms{centos7, isim, modelsim, xilinx13\_{}3, xilinx13\_{}4, xsim}
\input{\snippetpath/component_summary_table}

\section*{Functionality}
\begin{flushleft}
The File\_Write component writes application data to a file. It is normally used by
specifying an instance of the File\_Write component, and connecting its input port to an
output port of the component produces the data. The name of the file to be written is
specified in a property.
This component has one input port whose name is ``in'', which carries the messages to
be written to the file. There is no protocol associated with the port, enabling it to be
agnostic as to the protocol of the component connected to the input port.
\subsection*{Operating Modes}
\subsubsection*{Data Streaming Mode}
In data streaming mode, the contents of the file becomes the payloads of the stream of
messages arriving at the input port. No message lengths or opcodes are recorded in
the output file.
\newpage
\subsubsection*{Messaging Mode}
In messaging mode, the contents of the output file are written as a sequence of defined
messages, with an 8-byte header in the file itself preceding the data for each message
written to the file. This header contains the length and opcode of the message, with the
data contents of the message following the header. The length can be zero, meaning
that a header will be written but no data will follow the header in the file.\\
\medskip \medskip
\input{snippets/messaging_snippet}
\subsection*{No Protocol}
The port on the component has no protocol specified in order to support interfacing with any protocol.  This means that the created data file is formatted to match the protocol of the output port of the connected worker.
\subsection*{End of File Handling}
When the File\_Write component receives a end of file notification, it will interpret it as
the end of data and declare itself ``done'', and not write any further messages to the file.
\end{flushleft}

\section*{Worker Implementation Details}
\subsection*{\comp.hdl}
\begin{flushleft}
This worker will only run on simulator platforms.  This includes isim, xsim, and modelsim and will not run on or be built for any other hardware platforms.  This is because it contains code that cannot be realized into RTL.
\end{flushleft}
\subsection*{\comp.rcc}
\begin{flushleft}
This worker in implemented in the C language version of the RCC model.  Most new workers are implemented in the C++ language version of the RCC model.
\end{flushleft}
\newpage
\section*{Source Dependencies}
\subsection*{\comp.rcc}
\begin{itemize}
\item file\_write.c
\item file\_write.h
\end{itemize}
\subsection*{\comp.hdl}
\begin{itemize}
\item file\_write.vhd
\end{itemize}


\begin{landscape}
	\section*{Component Spec Properties}
	\begin{scriptsize}
\begin{tabular}{|p{2cm}|p{1.5cm}|c|c|c|p{1.5cm}|p{1cm}|p{7cm}|}
\hline
\rowcolor{blue}
Name                 & Type   & SequenceLength & ArrayDimensions & Accessibility       & Valid Range & Default & Usage
\\
\hline
fileName & string  length:1024& - & - & Initial & -  &- & The name of the file that is written to disk from the input port.
\\
\hline
messagesInFile & bool  & - & - & Initial & -  &false & The flag that is used to turn on and off message mode. See section on Message Mode.
\\
\hline
bytesWritten & uLongLong  & - & - & Volatile & -  &- & The number of bytes written to file.  Useful for debugging data flow issues.
\\
\hline
messagesWritten & uLongLong  & - & - & Volatile & -  &- & The number of messages written to file.  Useful for debugging data flow issues.
\\
\hline
stopOnEOF & bool  & - & - & Initial & -  &true & Property has no functionality, exists for backwards compatibility purposes.
\\
\hline
\end{tabular}
	\end{scriptsize}

	\section*{Worker Properties}
	\subsection*{\comp.hdl}
	\begin{scriptsize}
    \begin{tabular}{|p{2cm}|p{2.75cm}|p{3.5cm}|p{2cm}|p{2cm}|p{2.25cm}|p{1.5cm}|p{1cm}|p{4cm}|}
			\hline
			\rowcolor{blue}
			Type     & Name                      & Type  & SequenceLength & ArrayDimensions & Accessibility       & Valid Range & Default & Usage                                      \\
			\hline
			Spec Property & fileName & string  & - & - & Readback & -  &- & added Readback
            \\
            \hline
            Property & CWD\_MAX\_LENGTH & ulong  & - & - & Paramater & -  & 256 & parameter for max string length for the cwd property
            \\
            \hline
            Property & cwd & string length:CWD\_MAX\_LENGTH & - & - & Volatile & -  &- & the current working directory of the application (this is required for the hdl version of this worker and cannot be detiermined automaticly)
            \\
            \hline
    \end{tabular}
	\end{scriptsize}

	\subsection*{\comp.rcc}
	\begin{scriptsize}
    No properties changed from component specification.
	\end{scriptsize}

	\section*{Component Ports}
	\begin{scriptsize}
\begin{tabular}{|M{2cm}|M{1.5cm}|M{4cm}|c|c|M{9cm}|}
\hline
\rowcolor{blue}
Name & Producer & Protocol & Optional & Advanced & Usage
\\
\hline
in & False & None& False & - & -\\
\hline
\end{tabular}
	\end{scriptsize}

	\section*{Worker Interfaces}
	\subsection*{\comp.hdl}
	\begin{scriptsize}
\begin{tabular}{|M{2cm}|M{1.5cm}|M{4cm}|c|M{12cm}|}
\hline
\rowcolor{blue}
Type & Name & DataWidth & Advanced & Usage
\\
\hline
StreamInterface & in & 32 & - & -\\
\hline
\end{tabular}
	\end{scriptsize}

	\subsection*{\comp.rcc}
	\begin{scriptsize}
\begin{tabular}{|M{2cm}|M{1.5cm}|M{4cm}|c|M{12cm}|}
\hline
\rowcolor{blue}
Type & Name & DataWidth & Advanced & Usage
\\
\hline
Port & in & - & buffersize=8k & -\\
\hline
\end{tabular}
	\end{scriptsize}
\end{landscape}

\section*{Test and Verification}
\begin{flushleft}
All test benches use this worker as part of the verification process. A unit-test does not exist for this component.
\end{flushleft}
\end{document}
