// Define the zynq 7 series S_AXI_HP AXI interface
#define ADDR_WIDTH 32
#define ID_WIDTH 6
#define DATA_WIDTH 64
#define CLOCK_FROM_MASTER 1
