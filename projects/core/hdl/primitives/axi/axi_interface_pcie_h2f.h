// Define the generic PCIe h2f interface
#define ADDR_WIDTH 32
#define ID_WIDTH 6
#define DATA_WIDTH 64
#define RESET_FROM_MASTER 1
