// Define the zynq 7 series M_AXI_GP AXI interface
#define ADDR_WIDTH 32
#define ID_WIDTH 12
#define DATA_WIDTH 32
#define RESET_FROM_MASTER 1
