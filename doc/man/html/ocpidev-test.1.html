<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><title>ocpidev-test</title><link rel="stylesheet" type="text/css" href="docbook-xsl.css" /><meta name="generator" content="DocBook XSL Stylesheets Vsnapshot" /></head><body><div xml:lang="en" class="refentry" lang="en"><a id="idp140331704876208"></a><div class="titlepage"></div><div class="refnamediv"><h2>Name</h2><p>ocpidev-test — manage an OpenCPI component unit test suite.</p></div><div class="refsynopsisdiv"><a id="_synopsis"></a><h2>Synopsis</h2><p><span class="strong"><strong><code class="literal">ocpidev</code></strong></span> [<span class="emphasis"><em>&lt;options&gt;</em></span>] <span class="emphasis"><em>&lt;verb&gt;</em></span> <span class="strong"><strong><code class="literal">test</code></strong></span> <span class="emphasis"><em>&lt;name&gt;</em></span></p></div><div class="refsect1"><a id="_description"></a><h2>DESCRIPTION</h2><p>The <span class="strong"><strong>test</strong></span> noun represents an OpenCPI component unit test suite, which
is a collection of <span class="emphasis"><em>test cases</em></span> for testing all workers that implement the
a single component specification (OCS) across all available platforms for
which the workers have been built. The workers tested by the component
unit test suite can be written to different authoring models or languages or
can be different source code implementations of the same component
specification. As long as they implement the same component specification,
they can be tested with the same unit test suite.</p><p>A <span class="emphasis"><em>test case</em></span> is a parameterized test that uses a defined set of inputs
or generation scripts and a defined set of outputs or verification scripts
using a defined matrix of property values. OpenCPI supplies a default test case
that tests all parameter combinations as derived from all
worker parameter/build configurations or all workers. A developer can
supply runtime property settings with multiple values for each, resulting
in the cross-product of subcases. The default test case has one generation
script and one verification script, per port, parameterized by
<span class="emphasis"><em>test subcase</em></span> (specific test) property values.</p><p>A component unit test suite has five phases:</p><div class="itemizedlist"><ul class="itemizedlist" style="list-style-type: disc; "><li class="listitem">
Generate: generate testing artifacts after finding the OCS and the workers.
</li><li class="listitem">
Build: build HDL bitstream/executable artifacts for testing. This phase is
only meaningful for test suites that operate on HDL workers.
</li><li class="listitem">
Prepare: examine available built workers and available platforms,
creating execution scripts to use them all for executing feasible tests.
</li><li class="listitem">
Run: execute tests for all workers, configurations, test cases and platforms.
</li><li class="listitem">
Verify: verify results from the execution of tests on workers and platforms.
</li></ul></div><p>The <span class="strong"><strong><code class="literal">--mode=</code></strong></span> option to the <span class="strong"><strong><code class="literal">run</code></strong></span> operation for a test suite can
be used to set these phases. Note that HDL workers must be built before
generating/building tests for them.</p><p>A component unit test suite is described by an OpenCPI Test Suite
Description (OTSD), which is an XML file that specifies the test cases and
the defaults that apply to all test cases.</p><p>In a project, a component unit test suite resides in its own
directory named <span class="emphasis"><em>&lt;component-name&gt;</em></span><span class="strong"><strong><code class="literal">.test</code></strong></span> within a component library.
Within the test suite directory are:</p><div class="itemizedlist"><ul class="itemizedlist" style="list-style-type: disc; "><li class="listitem">
The test suite’s "make" file (named <span class="strong"><strong><code class="literal">Makefile</code></strong></span>)
</li><li class="listitem">
The Test Suite Description file (named <span class="emphasis"><em>&lt;component-name&gt;</em></span><span class="strong"><strong><code class="literal">-test.xml</code></strong></span>)
</li><li class="listitem">
A generator script to create input data files for ports or property value files (one generator script for each port) (initially named <span class="strong"><strong><code class="literal">generator.py</code></strong></span>)
</li><li class="listitem">
A verifier script to verify test output data produced by the output ports (one verifier script for each port) (initially named <span class="strong"><strong><code class="literal">verify.py</code></strong></span>)
</li><li class="listitem">
A viewer script to view the results of test execution; for example, a plot (initially named <span class="strong"><strong><code class="literal">view.sh</code></strong></span>)
</li></ul></div><p>The <span class="strong"><strong><code class="literal">ocpidev create</code></strong></span> operation produces initial versions of these files that can then be edited as necessary to implement the test suite.</p><p>Running the build and generate phases creates two additional subdirectories:</p><div class="itemizedlist"><ul class="itemizedlist" style="list-style-type: disc; "><li class="listitem">
A <span class="strong"><strong><code class="literal">gen/</code></strong></span> subdirectory that contains the built test cases and a text file that describes them
</li><li class="listitem">
An <span class="strong"><strong><code class="literal">applications/</code></strong></span> subdirectory that contains OpenCPI Application Specification (OAS) files and scripts used by the OpenCPI test framework to execute the test cases
</li></ul></div><p>Running the prepare and run phases creates an additional <span class="strong"><strong><code class="literal">run/</code></strong></span> subdirectory
that contains the results of the test case executions, such as output data
from each output port, final values of all properties, including volatile
properties, and a log file of the actual test execution.</p><p>Running <span class="strong"><strong><code class="literal">ocpidev clean</code></strong></span> removes the <span class="strong"><strong><code class="literal">application/</code></strong></span>, <span class="strong"><strong><code class="literal">gen/</code></strong></span> and <span class="strong"><strong><code class="literal">run</code>/</strong></span> subdirectories.</p><p>The chapter "Unit Testing of Workers" in the <span class="emphasis"><em>OpenCPI Component Development
Guide</em></span> provides details on the OpenCPI Test Suite Description XML structure
and syntax, test suite phase operations, and the development process for OpenCPI
component unit test suites.</p><p>Verbs that can operate on a component unit test suite are:</p><div class="variablelist"><dl class="variablelist"><dt><span class="term">
<span class="strong"><strong><code class="literal">build</code></strong></span>
</span></dt><dd>
    Build the component unit test suite, creating directories and files
    as necessary.
</dd><dt><span class="term">
<span class="strong"><strong><code class="literal">clean</code></strong></span>
</span></dt><dd>
    Remove all the generated and compiled files for the
    component unit test suite.
</dd><dt><span class="term">
<span class="strong"><strong><code class="literal">create</code></strong></span>
</span></dt><dd>
    Create the named component unit test suite, creating files and directories as required.
</dd><dt><span class="term">
<span class="strong"><strong><code class="literal">delete</code></strong></span>
</span></dt><dd>
    Remove all directories and files associated with the named
    component unit test suite.
</dd><dt><span class="term">
<span class="strong"><strong><code class="literal">run</code></strong></span>
</span></dt><dd>
    Run the component unit test suite, creating files and directories as required.
</dd></dl></div><p>Note that an asset’s name is implied by the directory you’re in when
you issue the <span class="strong"><strong><code class="literal">ocpidev</code></strong></span> command. For a component unit test suite, this
means that you can perform operations (verbs) on the unit test
without having to specify its
name when you issue the <span class="strong"><strong><code class="literal">ocpidev</code></strong></span> command from inside the
the component unit test suite’s directory.</p><p>The <span class="strong"><strong><code class="literal">ocpidev</code></strong></span> tool can also operate on a plural <span class="strong"><strong><code class="literal">tests</code></strong></span> noun.
The syntax is:</p><p><span class="strong"><strong><code class="literal">ocpidev</code></strong></span> [<span class="emphasis"><em>&lt;options&gt;</em></span>] [<span class="strong"><strong><code class="literal">run</code></strong></span>|<span class="strong"><strong><code class="literal">show</code></strong></span>] <span class="strong"><strong><code class="literal">tests</code></strong></span></p><p>The verbs that can be performed on the <span class="strong"><strong><code class="literal">tests</code></strong></span> noun are:</p><div class="variablelist"><dl class="variablelist"><dt><span class="term">
<span class="strong"><strong><code class="literal">run</code></strong></span>
</span></dt><dd>
    Run all the component unit tests suites in the given library.
    The general-purpose options and all <span class="strong"><strong><code class="literal">run</code></strong></span> options
    for component unit test suites can be used.
</dd><dt><span class="term">
<span class="strong"><strong><code class="literal">show</code></strong></span>
</span></dt><dd>
    Display information about component unit test suites in the current scope.
    The general-purpose options and the <span class="strong"><strong><code class="literal">show</code></strong></span> formatting options can
    be used.
</dd></dl></div></div><div class="refsect1"><a id="_options"></a><h2>OPTIONS</h2><p>In the descriptions below, a plus sign (+) after the option indicates
that it can be specified more than once on the command line.</p><p>Keywords for platforms supported by OpenCPI that can be
specified in the <span class="emphasis"><em>&lt;platform&gt;</em></span> argument to an option
can be found in the tables of supported platforms in the <span class="emphasis"><em>OpenCPI User Guide</em></span>.</p><p>Keywords for architectures supported by OpenCPI that can be specified
in the <span class="emphasis"><em>&lt;target&gt;</em></span> argument to an option include <span class="strong"><strong><code class="literal">isim</code></strong></span>, <span class="strong"><strong><code class="literal">modelsim</code></strong></span>,
<span class="strong"><strong><code class="literal">stratix4</code></strong></span>, <span class="strong"><strong><code class="literal">virtex6</code></strong></span>, <span class="strong"><strong><code class="literal">zynq</code></strong></span>.</p><div class="refsect2"><a id="_options_that_apply_to_all_operations_verbs_on_test_suites"></a><h3>Options That Apply to All Operations (Verbs) on Test Suites</h3><div class="variablelist"><dl class="variablelist"><dt><span class="term">
<span class="strong"><strong><code class="literal">-d</code></strong></span> <span class="emphasis"><em>&lt;directory&gt;</em></span>
</span></dt><dd>
    Specify the directory in which the command  should be run.
    Analogous to the <span class="strong"><strong>-C</strong></span> option in the POSIX <span class="strong"><strong>make</strong></span> command.
</dd><dt><span class="term">
<span class="strong"><strong><code class="literal">-v</code></strong></span>, <span class="strong"><strong><code class="literal">--verbose</code></strong></span>
</span></dt><dd>
    Describe what is happening in command execution in more detail.
</dd></dl></div></div><div class="refsect2"><a id="_options_when_creating_unit_test_suites"></a><h3>Options When Creating Unit Test Suites</h3><div class="variablelist"><dl class="variablelist"><dt><span class="term">
<span class="strong"><strong><code class="literal">--exclude-platform=</code></strong></span><span class="emphasis"><em>&lt;platform&gt;</em></span>, <span class="strong"><strong><code class="literal">-Q</code></strong></span> <span class="emphasis"><em>&lt;platform&gt;</em></span>+
</span></dt><dd>
    Do not build the component unit test suite for the specified platform.
</dd><dt><span class="term">
<span class="strong"><strong><code class="literal">--only-platform=</code></strong></span><span class="emphasis"><em>&lt;platform&gt;</em></span>, <span class="strong"><strong><code class="literal">-G</code></strong></span> <span class="emphasis"><em>&lt;platform&gt;</em></span>+
</span></dt><dd>
    Only build the component unit test suite for the specified platform.
</dd><dt><span class="term">
<span class="strong"><strong><code class="literal">-S</code></strong></span> <span class="emphasis"><em>&lt;component-spec&gt;</em></span>
</span></dt><dd>
    Specify the component spec (OCS) that the component unit
    test suite implements.
    The default is <span class="emphasis"><em>&lt;name&gt;</em></span><span class="strong"><strong><code class="literal">-spec</code></strong></span> or <span class="emphasis"><em>&lt;name&gt;</em></span><span class="strong"><strong><code class="literal">_spec</code></strong></span>
    depending on what <span class="strong"><strong><code class="literal">ocpidev</code></strong></span> finds in the <span class="strong"><strong><code class="literal">specs</code></strong></span> directory
    of the library or project (or libraries specified with the <span class="strong"><strong><code class="literal">-y</code></strong></span>
    option or other projects specified by the <span class="strong"><strong><code class="literal">-D</code></strong></span> option when the
    project that contains the component unit test suite was created).
</dd><dt><span class="term">
<span class="strong"><strong><code class="literal">-T</code></strong></span> <span class="emphasis"><em>&lt;target&gt;</em></span>+
</span></dt><dd>
    Only build the component unit test suite for the specified architecture.
</dd><dt><span class="term">
<span class="strong"><strong>-k</strong></span>
</span></dt><dd>
    Keep files and directories created after a component unit test suite
    creation fails. Normally, all such files and directories are removed
    on any failure.
</dd></dl></div></div><div class="refsect2"><a id="_options_when_deleting_unit_test_suites"></a><h3>Options When Deleting Unit Test Suites</h3><div class="variablelist"><dl class="variablelist"><dt><span class="term">
<span class="strong"><strong>-f</strong></span>
</span></dt><dd>
    Force deletion: do not ask for confirmation when deleting
    a component unit test suite. Normally, you are asked to confirm a deletion.
</dd></dl></div></div><div class="refsect2"><a id="_options_when_building_component_unit_test_suites"></a><h3>Options When Building Component Unit Test Suites</h3><div class="variablelist"><dl class="variablelist"><dt><span class="term">
<span class="strong"><strong><code class="literal">--hdl-rcc-platform=</code></strong></span><span class="emphasis"><em>&lt;platform&gt;</em></span>+
</span></dt><dd>
    Build the component unit test(s) for the RCC platforms
    associated with the specified HDL platform. If this
    option is not used (and <span class="strong"><strong><code class="literal">--rcc-platform</code></strong></span> <span class="emphasis"><em>&lt;platform&gt;</em></span> is also not used),
    the current development software platform is used as
    the single RCC platform used for building.
</dd><dt><span class="term">
<span class="strong"><strong><code class="literal">--rcc-platform=</code></strong></span><span class="emphasis"><em>&lt;platform&gt;</em></span>+
</span></dt><dd>
    Build the component unit test(s) for the specified RCC platform. If this
    option is not used (and <span class="strong"><strong><code class="literal">--hdl-rcc-platform</code></strong></span> <span class="emphasis"><em>&lt;platform&gt;</em></span>
    is also not used), the current development software platform
    is used as the single RCC platform used for building.
</dd></dl></div></div><div class="refsect2"><a id="_options_when_building_component_unit_test_suites_with_hdl_workers"></a><h3>Options When Building Component Unit Test Suites with HDL Workers</h3><div class="variablelist"><dl class="variablelist"><dt><span class="term">
<span class="strong"><strong><code class="literal">--hdl-platform=</code></strong></span><span class="emphasis"><em>&lt;hdl-platform&gt;</em></span>+
</span></dt><dd>
    Build the component unit test suite(s) for the specified HDL platform.
</dd><dt><span class="term">
<span class="strong"><strong><code class="literal">--hdl-target=</code></strong></span><span class="emphasis"><em>&lt;target&gt;</em></span>+
</span></dt><dd>
    Build the component unit test suite(s) for the specified HDL architecture.
    If only HDL targets are specified (and no HDL platforms), containers for
    the unit test suite(s) are not built.
</dd></dl></div></div><div class="refsect2"><a id="_options_when_running_component_unit_tests_suites"></a><h3>Options When Running Component Unit Tests Suites</h3><div class="variablelist"><dl class="variablelist"><dt><span class="term">
<span class="strong"><strong><code class="literal">--accumulate-errors</code></strong></span>
</span></dt><dd>
    Report execution or verification errors as they occur rather
    than ending the test on the first failure detected.
</dd><dt><span class="term">
<span class="strong"><strong><code class="literal">--case=</code></strong></span><span class="emphasis"><em>&lt;test-case&gt;</em></span>+
</span></dt><dd>
    Specify the test case(s) to be run and verified. You can use
    the wildcard character in <span class="emphasis"><em>&lt;test-case&gt;</em></span>; for example, case*, case0.0*.
</dd><dt><span class="term">
<span class="strong"><strong><code class="literal">--exclude-platform=</code></strong></span><span class="emphasis"><em>&lt;platform&gt;</em></span>, <span class="strong"><strong><code class="literal">-Q</code></strong></span> <span class="emphasis"><em>&lt;platform&gt;</em></span>+
</span></dt><dd>
    Specify the runtime platform to omit from the component unit test suite(s).
</dd><dt><span class="term">
<span class="strong"><strong><code class="literal">--hdl-platform=</code></strong></span><span class="emphasis"><em>&lt;platform&gt;</em></span>+
</span></dt><dd>
    Specify the HDL platform to use with the component unit test suite(s).
    This option is only valid in generate and build phases.
</dd><dt><span class="term">
<span class="strong"><strong><code class="literal">--keep-simulations</code></strong></span>
</span></dt><dd>
    Keep HDL simulation files regardless of verification results.
    By default, simulation files are removed if the verification
    is successful. Warning: Simulation files can become large!
</dd><dt><span class="term">
<span class="strong"><strong><code class="literal">--library=</code></strong></span><span class="emphasis"><em>&lt;library&gt;</em></span>, <span class="strong"><strong><code class="literal">-l</code></strong></span> <span class="emphasis"><em>&lt;library&gt;</em></span>
</span></dt><dd>
    Run the component unit test suite(s) in the specified library.
</dd><dt><span class="term">
<span class="strong"><strong><code class="literal">--mode=</code></strong></span><span class="emphasis"><em>&lt;mode&gt;</em></span>[,<span class="emphasis"><em>&lt;mode&gt;</em></span>[,<span class="emphasis"><em>&lt;mode&gt;</em></span>…]]
</span></dt><dd><p class="simpara">
    Specify which phase(s) of the component unit test suite(s) to execute. Valid modes are:
</p><div class="itemizedlist"><ul class="itemizedlist" style="list-style-type: disc; "><li class="listitem">
<span class="strong"><strong>all</strong></span>:  execute all five phases (the default if <span class="strong"><strong><code class="literal">--mode</code></strong></span> is not used)
</li><li class="listitem">
<span class="strong"><strong>gen</strong></span>: execute generate phase
</li><li class="listitem">
<span class="strong"><strong>gen_build</strong></span>: execute generate and build phases. This mode is
      analagous to the <span class="strong"><strong><code class="literal">build</code></strong></span> verb.
</li><li class="listitem">
<span class="strong"><strong>prep_run_verify</strong></span>: execute prepare, run, and verify phases
</li><li class="listitem">
<span class="strong"><strong>prep</strong></span>: execute prepare phase
</li><li class="listitem">
<span class="strong"><strong>run</strong></span>: execute run phase
</li><li class="listitem">
<span class="strong"><strong>prep_run</strong></span>: execute prepare and run phases
</li><li class="listitem">
<span class="strong"><strong>verify</strong></span>: execute verify phase
</li><li class="listitem">
<span class="strong"><strong>view</strong></span>: execute the view script (view.sh) on an already executed run
</li><li class="listitem">
<span class="strong"><strong>clean_all</strong></span>: clean all generated files. This mode is analogous to the <span class="strong"><strong><code class="literal">clean</code></strong></span> verb.
</li><li class="listitem">
<span class="strong"><strong>clean_run</strong></span>: clean all files generated during the run phase
</li></ul></div></dd><dt><span class="term">
<span class="strong"><strong><code class="literal">--only-platform=</code></strong></span><span class="emphasis"><em>&lt;platform&gt;</em></span>, <span class="strong"><strong><code class="literal">-G</code></strong></span> <span class="emphasis"><em>&lt;platform&gt;</em></span>+
</span></dt><dd>
    Specify the runtime platform to use with the component unit test suite(s).
</dd><dt><span class="term">
<span class="strong"><strong><code class="literal">--rcc-platform=</code></strong></span><span class="emphasis"><em>&lt;platform&gt;</em></span>+
</span></dt><dd>
    Specify the RCC platform to use to build and generate the component unit test suite(s).
</dd><dt><span class="term">
<span class="strong"><strong><code class="literal">--remotes=</code></strong></span><span class="emphasis"><em>&lt;system&gt;</em></span>
</span></dt><dd>
    Specify the remote system to run the component unit
    test suite(s) by setting the
    <span class="strong"><strong><code class="literal">OCPI_REMOTE_TEST_SYS</code></strong></span> variable. See the section
    "Defining Remote Systems for Executing Tests"
    in the <span class="emphasis"><em>OpenCPI Component Development Guide</em></span> for more information.
</dd><dt><span class="term">
<span class="strong"><strong><code class="literal">--view</code></strong></span>
</span></dt><dd>
    Run the view script (<span class="strong"><strong><code class="literal">view.sh</code></strong></span>) at the conclusion of the
    test suite’s execution.
</dd></dl></div></div><div class="refsect2"><a id="_options_when_showing_component_unit_test_suites_plural_noun_only"></a><h3>Options When Showing Component Unit Test Suites (plural noun only)</h3><div class="variablelist"><dl class="variablelist"><dt><span class="term">
<span class="strong"><strong>--json</strong></span>
</span></dt><dd>
    Format the output in Javascript Object Notation (JSON) format
    for integration with other software.
</dd><dt><span class="term">
<span class="strong"><strong>--local-scope</strong></span>
</span></dt><dd>
    Only display information about the component unit tests in the local library.
</dd><dt><span class="term">
<span class="strong"><strong>--simple</strong></span>
</span></dt><dd>
    Format the output as simply as possible.
</dd><dt><span class="term">
<span class="strong"><strong>--table</strong></span>
</span></dt><dd>
    Display the output in an easy-to-read table.
    This is the default display format
    used if <span class="strong"><strong>--simple</strong></span> or <span class="strong"><strong>--json</strong></span> are not used.
</dd></dl></div></div></div><div class="refsect1"><a id="_examples"></a><h2>EXAMPLES</h2><div class="orderedlist"><ol class="orderedlist" type="1"><li class="listitem"><p class="simpara">
From the project’s <span class="strong"><strong><code class="literal">components</code></strong></span> library, create a component
unit test suite for the <span class="strong"><strong><code class="literal">mycomp</code></strong></span> component:
</p><pre class="screen">ocpidev create test mycomp</pre></li><li class="listitem"><p class="simpara">
Inside the <span class="strong"><strong><code class="literal">components</code></strong></span> library, create a test suite for the <span class="strong"><strong><code class="literal">mycomp</code></strong></span>
component that only runs on the <span class="strong"><strong><code class="literal">centos7</code></strong></span> platform:
</p><pre class="screen">ocpidev create test mycomp --only-platform=centos7</pre></li><li class="listitem"><p class="simpara">
Inside the <span class="strong"><strong><code class="literal">components</code></strong></span> library, create a test suite for the <span class="strong"><strong><code class="literal">mycomp</code></strong></span>
component that only runs on platforms that implement the <span class="strong"><strong><code class="literal">zynq</code></strong></span> architecture:
</p><pre class="screen">ocpidev create test mycomp -T zynq</pre></li><li class="listitem"><p class="simpara">
Inside the <span class="strong"><strong><code class="literal">mycomp.test</code></strong></span> directory, build the component
unit test suite for the <span class="strong"><strong><code class="literal">zed</code></strong></span> HDL platform and <span class="strong"><strong><code class="literal">xilinx13_3</code></strong></span> RCC platform.
</p><pre class="screen">ocpidev build test --hdl-platform zed --rcc-platform xilinx13_3</pre></li><li class="listitem"><p class="simpara">
Inside the <span class="strong"><strong><code class="literal">mycomp.test</code></strong></span> directory, clean the directory:
</p><pre class="screen">ocpidev clean test</pre></li><li class="listitem"><p class="simpara">
Inside the <span class="strong"><strong><code class="literal">components</code></strong></span> library in the current project, run
the prepare, run and verify phases of the unit test suite for
the <span class="strong"><strong><code class="literal">mycomp</code></strong></span> component, accumulating any errors and invoking
the viewer shell script (<span class="strong"><strong><code class="literal">view.sh</code></strong></span>) to view the plotted test results:
</p><pre class="screen">ocpidev run test mycomp.test --accumulate_errors
                             --mode=prep_run_verify
                             --view</pre></li><li class="listitem"><p class="simpara">
Inside the <span class="strong"><strong><code class="literal">assets</code></strong></span> project, run the test suite for the timestamper
component in the <span class="strong"><strong><code class="literal">util_comps</code></strong></span> library:
</p><pre class="screen">ocpidev run test timestamper.test --library=util_comps</pre></li><li class="listitem"><p class="simpara">
Inside the <span class="strong"><strong><code class="literal">components</code></strong></span> library in the current project, run
all the component unit test suites in the library:
</p><pre class="screen">ocpidev run tests</pre></li><li class="listitem"><p class="simpara">
Display information about the component unit test suites in the current project:
</p><pre class="screen">ocpidev show tests --local-scope</pre></li></ol></div></div><div class="refsect1"><a id="_bugs"></a><h2>BUGS</h2><p>See <a class="ulink" href="https://www.opencpi.org/report-defects" target="_top">https://www.opencpi.org/report-defects</a></p></div><div class="refsect1"><a id="_resources"></a><h2>RESOURCES</h2><p>See the main web site: <a class="ulink" href="https://www.opencpi.org" target="_top">https://www.opencpi.org</a></p></div><div class="refsect1"><a id="_see_also"></a><h2>SEE ALSO</h2><p><a class="ulink" href="ocpidev.1.html" target="_top">ocpidev(1)</a>
<a class="ulink" href="ocpidev-application.1.html" target="_top">ocpidev-application(1)</a>
<a class="ulink" href="ocpidev-build.1.html" target="_top">ocpidev-build(1)</a>
<a class="ulink" href="ocpidev-component.1.html" target="_top">ocpidev-component(1)</a>
<a class="ulink" href="ocpidev-create.1.html" target="_top">ocpidev-create(1)</a>
<a class="ulink" href="ocpidev-clean.1.html" target="_top">ocpidev-clean(1)</a>
<a class="ulink" href="ocpidev-delete.1.html" target="_top">ocpidev-delete(1)</a>
<a class="ulink" href="ocpidev-project.1.html" target="_top">ocpidev-project(1)</a>
<a class="ulink" href="ocpidev-run.1.html" target="_top">ocpidev-run(1)</a>
<a class="ulink" href="ocpidev-show.1.html" target="_top">ocpidev-show(1)</a>
<a class="ulink" href="ocpidev-worker.1.html" target="_top">ocpidev-worker(1)</a></p></div><div class="refsect1"><a id="_copying"></a><h2>COPYING</h2><p>Copyright (C) 2020 OpenCPI www.opencpi.org. OpenCPI is free software:
you can redistribute it and/or modify it under the terms of the
GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your
option) any later version.</p></div></div></body></html>